
# Configure the OpenStack Provider

//provider "openstack" {
//  user_name   = "${var.admin_name}"
//  tenant_name = "${var.admin_name}"
//  password    = "${var.admin_password}"
//  auth_url    = "${var.auth_url}"
//  region      = "${var.region}"
//  domain_name = "${var.domain_name}"
//}
provider "openstack" {
  user_name   = "${var.user_name}"
  tenant_name = "${var.user_name}"
  password    = "${var.user_password}"
  auth_url    = "${var.auth_url}"
  region      = "${var.region}"
  domain_name = "${var.domain_name}"
}