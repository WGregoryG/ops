

resource "openstack_images_image_v2" "debian8" {
  name   = "debian-8"
  image_source_url = "http://cdimage.debian.org/cdimage/openstack/current-8/debian-8-openstack-amd64.qcow2"
  container_format = "bare"
  disk_format = "qcow2"
  visibility = "public"

}