

resource "openstack_images_image_v2" "centos7" {
  name   = "centos-7"
  image_source_url = "http://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
  container_format = "bare"
  disk_format = "qcow2"
  visibility = "public"
}