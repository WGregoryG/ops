

resource "openstack_images_image_v2" "debian9" {
  name   = "debian-9"
  image_source_url = "http://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd64.qcow2"
  container_format = "bare"
  disk_format = "qcow2"
  visibility = "public"

}