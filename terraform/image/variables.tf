# variable for project

variable "admin_name" {}
variable "user_name" {}
variable "tenant_name" {}
variable "admin_password" {}
variable "user_password" {}
variable "auth_url" {}
variable "region"  {}
variable "domain_name" {}
variable network_provider_uuid {
    description = "Uuid of the provider network"
    default = ""
}
variable network_selfservice_uuid {
    description = "Uuid of the selfservice network"
    default = ""
}
variable subnet_provider_uuid {
    description = "Uuid of the provider subnet"
    default = ""
}
variable subnet_selfservice_uuid {
    description = "Uuid of the selfservice subnet"
    default = ""
}

variable port_elaticsearch_01 {
    description = "Uuid of elasticsearch 01 port"
    default = "e03d737a-eb28-425d-be9b-f2ba7d55d7f6"
}

variable port_elaticsearch_02 {
    description = "Uuid of elasticsearch 02 port"
    default = "e52c8b17-5113-4e04-b669-b7f7e138d019"
}

variable port_elaticsearch_03 {
    description = "Uuid of elasticsearch 03 port"
    default = "3f0d750e-a6af-4a43-ab54-822d26d95236"
}
