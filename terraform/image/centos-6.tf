

resource "openstack_images_image_v2" "centos6" {
  name   = "centos-6"
  image_source_url = "http://cloud.centos.org/centos/6/images/CentOS-6-x86_64-GenericCloud.qcow2"
  container_format = "bare"
  disk_format = "qcow2"
  visibility = "public"
}