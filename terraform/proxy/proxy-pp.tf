
resource "openstack_compute_instance_v2" "proxy_01" {
  name      	= "proxy-pp-01"
  region    	= "${var.region}"
  image_name	= "centos-6"
  flavor_name   = "normal"
  key_pair      = "greg"

  network {
    uuid = "${openstack_networking_port_v2.proxy_pp_01.network_id}"
    name = "provider"
    port = "${openstack_networking_port_v2.proxy_pp_01.id}"
  }

  provisioner "file" {
    source      = "conf/bootstrap.sh"
    destination = "/tmp/bootstrap.sh"
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
  }
  provisioner "remote-exec" {
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
    inline = [
      "chmod +x /tmp/bootstrap.sh && sudo /tmp/bootstrap.sh"
    ]
  }
}



resource "openstack_compute_instance_v2" "proxy_02" {
  name      	= "proxy-pp-02"
  region    	= "${var.region}"
  image_name	= "centos-6"
  flavor_name   = "normal"
  key_pair      = "greg"

  network {
    uuid = "${openstack_networking_port_v2.proxy_pp_02.network_id}"
    name = "provider"
    port = "${openstack_networking_port_v2.proxy_pp_02.id}"
  }

  provisioner "file" {
    source      = "conf/bootstrap.sh"
    destination = "/tmp/bootstrap.sh"
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
  }
  provisioner "remote-exec" {
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
    inline = [
      "chmod +x /tmp/bootstrap.sh && sudo /tmp/bootstrap.sh"
    ]
  }
}
