#conf security admin

resource "openstack_compute_secgroup_v2" "secgroup_proxy_access" {
  name        = "secgroup_proxy"
  description = "proxy security group"

  rule {
    from_port   = "-1"
    to_port     = "-1"
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 3128
    to_port     = 3128
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}
