# wister frontend configuration

resource "openstack_compute_instance_v2" "wister-01" {
  name      	= "wister-01"
  region    	= "WisterBureau"
  image_name	= "debian-9"
  flavor_name   = "normal"
  key_pair      = "greg"
  security_groups = ["default"]
  network {
      uuid = "${var.network_provider_uuid}"
      name = "provider"
  }
}
