
resource "openstack_networking_port_v2" "logforwarder_01" {
  name               = "logforwarder-01"
  network_id         = "${var.network_provider_uuid}"
  admin_state_up     = "true"
  device_owner       = "wister"

  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_logfowarder_access.id}"]
  fixed_ip {
    "subnet_id"  = "${var.subnet_provider_uuid}"
    "ip_address" = "172.16.0.50"
  }
}
