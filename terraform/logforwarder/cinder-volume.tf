
resource "openstack_blockstorage_volume_v2" "cinder_logforwarder_01" {
  region      = "${var.region}"
  name        = "logforwader-volume-01"
  description = "data volume for forwarder node"
  size        = 500
}

