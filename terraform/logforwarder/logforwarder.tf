


resource "openstack_compute_instance_v2" "logforwarer_01" {
  name      	= "logforwarder-01"
  region    	= "${var.region}"
  image_name	= "debian-9.2.0"
  flavor_name   = "normal"
  key_pair      = "greg"
//  security_groups = ["secgroup_elasticsearch"]

  network {
    uuid = "${openstack_networking_port_v2.logforwarder_01.network_id}"
    name = "provider"
    port = "${openstack_networking_port_v2.logforwarder_01.id}"
  }

  provisioner "file" {
    source      = "conf/bootstrap.sh"
    destination = "/tmp/bootstrap.sh"
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
  }

  provisioner "remote-exec" {
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
    inline = [
      "chmod +x /tmp/bootstrap.sh && sudo /tmp/bootstrap.sh"
    ]
  }
}






resource "openstack_compute_volume_attach_v2" "va_1" {
  instance_id = "${openstack_compute_instance_v2.logforwarer_01.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.cinder_logforwarder_01.id}"
}

