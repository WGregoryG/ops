#conf security admin

resource "openstack_compute_secgroup_v2" "secgroup_logfowarder_access" {
  name        = "secgroup_elasticsearch"
  description = "basic security group"

  rule {
    from_port   = "-1"
    to_port     = "-1"
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 5601
    to_port     = 5601
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 9000
    to_port     = 9000
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 9200
    to_port     = 9300
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}
