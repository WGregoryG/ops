
resource "openstack_compute_flavor_v2" "extralarge" {
  name  = "extralarge"
  ram   = "8192"
  vcpus = "4"
  disk  = "80"
  is_public = "true"
}