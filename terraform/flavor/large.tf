
resource "openstack_compute_flavor_v2" "large" {
  name  = "large"
  ram   = "4096"
  vcpus = "4"
  disk  = "20"
  is_public = "true"
}