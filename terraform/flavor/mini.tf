
resource "openstack_compute_flavor_v2" "mini" {
  name  = "mini"
  ram   = "1024"
  vcpus = "1"
  disk  = "10"
  is_public = "true"
}