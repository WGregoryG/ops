
resource "openstack_compute_flavor_v2" "normal" {
  name  = "normal"
  ram   = "2048"
  vcpus = "2"
  disk  = "20"
  is_public = "true"
}