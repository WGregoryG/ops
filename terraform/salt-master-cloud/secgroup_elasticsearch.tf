#conf security admin

resource "openstack_compute_secgroup_v2" "secgroup_saltmaster_access" {
  name        = "secgroup_saltmaster"
  description = "salt security group"

  rule {
    from_port   = "-1"
    to_port     = "-1"
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 4505
    to_port     = 4506
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

}
