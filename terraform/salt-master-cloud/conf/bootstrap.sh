#!/bin/bash

IP_SALT_MASTER="172.16.0.42"
# lancé en root on peut avancer
cd

wget -O - https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/apt/debian/9/amd64/latest stretch main" > /etc/apt/sources.list.d/saltstack.list

apt-get update
apt-get dist-upgrade -y
apt-get install -y git htop curl apt-transport-https salt-master

git clone https://github.com/wako057/nuxminimal.git && nuxminimal/init.sh
IP_LAN=`hostname -I`

sed -i -e "s/\#interface\:\ 0\.0\.0\.0/interface\:\ $IP_LAN/g" /etc/salt/master

reboot