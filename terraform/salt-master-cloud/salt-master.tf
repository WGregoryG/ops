


resource "openstack_compute_instance_v2" "saltmastercloud" {
  name      	= "salt-master-cloud"
  region    	= "${var.region}"
  image_name	= "debian-9"
  flavor_name   = "normal"
  key_pair      = "greg"

  network {
    uuid = "${var.network_provider_uuid}"
    name = "provider"
    port = "${openstack_networking_port_v2.salt_master_cloud.id}"
  }

  provisioner "file" {
    source      = "conf/bootstrap.sh"
    destination = "/tmp/bootstrap.sh"
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
  }

  provisioner "remote-exec" {
    connection {
      user = "${var.ssh_user_name}"
      private_key = "${file("${var.ssh_key_file}")}"
      agent = false
    }
    inline = [
      "chmod +x /tmp/bootstrap.sh && sudo /tmp/bootstrap.sh"
    ]
  }

}

