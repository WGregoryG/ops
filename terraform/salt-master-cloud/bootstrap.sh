#!/bin/bash

sudo su
# lancé en root on peut avancer
cd
apt-get update
apt-get dist-upgrade -y
apt-get install -y git htop curl apt-transport-https

wget -O - https://repo.saltstack.com/apt/debian/8/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/apt/debian/8/amd64/latest jessie main" > /etc/apt/sources.list.d/saltstack.list

apt-get update
apt-get install -y salt-master
IP_LAN=`hostname -I`

sed -i -e "s/\#interface\:\ 0\.0\.0\.0/interface\:\ $IP_LAN/g" /etc/salt/master
/etc/init.d/salt-master restart

git clone https://github.com/wako057/nuxminimal.git && nuxminimal/init.sh
