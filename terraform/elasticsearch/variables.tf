# variable for project


variable "admin_name" {}
variable "user_name" {}
variable "tenant_name" {}
variable "admin_password" {}
variable "user_password" {}
variable "auth_url" {}
variable "region"  {}
variable "domain_name" {}
variable network_provider_uuid {
    description = "Uuid of the provider network"
    default = ""
}
variable network_selfservice_uuid {
    description = "Uuid of the selfservice network"
    default = ""
}
variable subnet_provider_uuid {
    description = "Uuid of the provider subnet"
    default = ""
}
variable subnet_selfservice_uuid {
    description = "Uuid of the selfservice subnet"
    default = ""
}

variable "ssh_key_file" {
    default = "conf/id_rsa"
}

variable "ssh_user_name" {
    default = "debian"
}