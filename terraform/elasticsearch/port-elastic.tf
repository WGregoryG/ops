
resource "openstack_networking_port_v2" "elasticsearch_01" {
  name               = "elasticsearch-01"
  network_id         = "${var.network_provider_uuid}"
  admin_state_up     = "true"
  device_owner       = "wister"

  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_elasticsearch_access.id}"]
  fixed_ip {
    "subnet_id"  = "${var.subnet_provider_uuid}"
    "ip_address" = "172.16.0.51"
  }
}

resource "openstack_networking_port_v2" "elasticsearch_02" {
  name               = "elasticsearch-02"
  network_id         = "${var.network_provider_uuid}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_elasticsearch_access.id}"]
  device_owner       = "wister"
  fixed_ip {
    "subnet_id"  = "${var.subnet_provider_uuid}"
    "ip_address" = "172.16.0.52"
  }
}

resource "openstack_networking_port_v2" "elasticsearch_03" {
  name               = "elasticsearch-03"
  network_id         = "${var.network_provider_uuid}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_elasticsearch_access.id}"]
  device_owner       = "wister"
  fixed_ip {
    "subnet_id"  = "${var.subnet_provider_uuid}"
    "ip_address" = "172.16.0.53"
  }
}
