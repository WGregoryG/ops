
resource "openstack_blockstorage_volume_v2" "cinder_elastic_01" {
  region      = "${var.region}"
  name        = "elasticearch-volume-01"
  description = "data volume for elastic node"
  size        = 500
}

resource "openstack_blockstorage_volume_v2" "cinder_elastic_02" {
  region      = "${var.region}"
  name        = "elasticearch-volume-02"
  description = "data volume for elastic node"
  size        = 500
}

resource "openstack_blockstorage_volume_v2" "cinder_elastic_03" {
  region      = "${var.region}"
  name        = "elasticearch-volume-03"
  description = "data volume for elastic node"
  size        = 500
}
