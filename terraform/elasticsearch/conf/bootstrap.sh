#!/bin/bash

IP_SALT_MASTER="172.16.0.42"
# lancé en root on peut avancer
cd

##### Montage Disque Dur /data ####
#mkdir /data
#mkfs.ext3 /dev/vdb
#echo `blkid /dev/vdb | awk '{print$2}' | sed -e 's/"//g'` /data               ext2    errors=remount-ro 0       1 >> /etc/fstab
#mount /data


wget -O - https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/apt/debian/9/amd64/latest stretch main" > /etc/apt/sources.list.d/saltstack.list

apt-get update
apt-get dist-upgrade -y
apt-get install -y git htop curl apt-transport-https salt-minion

git clone https://github.com/wako057/nuxminimal.git && nuxminimal/init.sh


sed -i -e "s/\#master\:\ salt/master\:\ $IP_SALT_MASTER/g" /etc/salt/minion
reboot