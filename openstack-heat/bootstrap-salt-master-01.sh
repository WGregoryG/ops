#!/bin/bash

#sudo su
# lancé en root on peut avancer
#cd
apt-get update
#apt-get dist-upgrade -y
apt-get install -y git htop curl apt-transport-https

#wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
#echo "deb https://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list

wget -O - https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/apt/debian/9/amd64/latest jessie main" > /etc/apt/sources.list.d/saltstack.list

apt-get update
apt-get install -y salt-master
#apt install -t jessie-backports  openjdk-8-jre-headless ca-certificates-java
#salt-api  python-openssl
#salt-call --local tls.create_self_signed_cert
IP_LAN=`hostname -I`

sed -i -e "s/\#interface\:\ 0\.0\.0\.0/interface\:\ $IP_LAN/g" /etc/salt/master
/etc/init.d/salt-master restart

git clone https://github.com/wako057/nuxminimal.git && nuxminimal/init.sh

wc_notify --data-binary '{"status": "SUCCESS"}'
#echo "On demande le reboot pour finaliser"
#reboot