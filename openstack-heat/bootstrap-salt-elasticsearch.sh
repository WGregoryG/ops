#!/bin/bash

IP_SALT_MASTER="192.168.3.60"
#IP_SALT_MASTER=$1
#
#if [$# -ne 1];
#    then echo "illegal number of parameters"
#    exit 1;
#fi

sudo su
# lancé en root on peut avancer
cd

wget -O - https://repo.saltstack.com/apt/debian/8/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
echo "deb http://repo.saltstack.com/apt/debian/8/amd64/latest jessie main" > /etc/apt/sources.list.d/saltstack.list

apt-get update
apt-get dist-upgrade -y
apt-get install -y git htop curl salt-minion
git clone https://github.com/wako057/nuxminimal.git && nuxminimal/init.sh

mkdir /data

##### Montage Disque Dur /data ####

#mkfs.ext3 /dev/vdb
#echo `blkid /dev/vdb | awk '{print$2}' | sed -e 's/"//g'` /data               ext2    errors=remount-ro 0       1 >> /etc/fstab
#mount /data

#### SaltStack ####

#echo "deb http://repo.saltstack.com/apt/debian/8/amd64/latest jessie main" > saltstack.list
#sudo mv saltstack.list /etc/apt/sources.list.d/

sed -i -e "s/\#master\:\ salt/master\:\ $IP_SALT_MASTER/g" /etc/salt/minion
/etc/init.d/salt-minion restart

#echo "On demande le reboot pour finaliser"
#reboot