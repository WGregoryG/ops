#!/bin/bash

salt '*' state.apply openstack.stop-services
salt '*' state.apply database.recreatedb
salt '*' state.apply keystone
salt '*' state.apply glance
salt '*' state.apply nova
salt '*' state.apply neutron
salt '*' state.apply dashboard
salt '*' state.apply cinder
salt '*' state.apply openstack.restart-services
salt '*' state.apply openstack.network-create-selfservice
salt '*' state.apply openstack.network-create-provider

cd /root/terraform/ssh && terraform apply
cd /root/terraform/flavor && terraform apply
cd /root/terraform/image && terraform apply
